# **<span style="color:red">Te quiero</span>**

*<span style="color:green">No te digo algo bonito,*

*<span style="color:green">te digo algo sincero,*

*<span style="color:green">mi cariño es infinito*

*<span style="color:green">y mi amor es verdadero.*

!["ilustracion"](https://us.123rf.com/450wm/yupiramos/yupiramos1803/yupiramos180307685/96803395-hermosa-flor-gui%C3%B1o-ilustraci%C3%B3n-vectorial-de-dibujos-animados.jpg)